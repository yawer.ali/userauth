import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
// import { useMutation } from "react-query";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const schema = yup
  .object({
    email: yup
      .string()
      .required("Email is required")
      .matches(/^\S/, "First character cannot be Space ")
      .matches(
        /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i,
        "valid email"
      )
      .email("Please enter a valid Email")
      .max(255),
    password: yup
      .string()
      .required("Please Enter your password")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
      ),
  })
  .required();

export default function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  // const { error } = useMutation(Login1);
  // console.log(error);
  const navigate = useNavigate();
  const onSubmit = (values) => {
    console.log(values);
    axios
      .post("https://sql-dev-india.thewitslab.com:3003/auth/login", values)
      .then((response) => {
        const userToken = response.data.token;
        localStorage.setItem("token", userToken);
        userToken ? navigate("/Dashboard") : navigate("/");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container mt-5">
      <h3 className="text-center">Login Page</h3>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input type="email" className="form-control" {...register("email")} />
          <p className="text-danger">{errors.email?.message}</p>
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            {...register("password")}
          />
          <p className="text-danger">{errors.password?.message}</p>
        </div>

        <button type="submit" className="btn btn-primary form-control">
          Submit
        </button>
      </form>
    </div>
  );
}
