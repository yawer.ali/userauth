import React from "react";
import { useForm } from "react-hook-form";
import "../App.css";
import Navbar from "./Navbar";

export default function Formv() {
  const {
    register,
    handleSubmit,
    // watch,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    alert(JSON.stringify(data));
  }; // Our form submit function which will invoke after successful validation

  // console.log(watch("example")); // Here We can watch individual input As We pass the name of the input

  return (
    <>
      <Navbar />
      <div className="container text-center mt-3">
        <form onSubmit={handleSubmit(onSubmit)} className="field">
          <label className="mt-2">First Name</label>
          <input
            className="form-control"
            {...register("firstName", {
              pattern: /^[A-Za-z]+$/i,
              required: true,
              maxLength: 10,
            })}
          />
          {errors?.firstName?.type === "required" && (
            <p className="text-danger">This field is required</p>
          )}
          {errors?.firstName?.type === "maxLength" && (
            <p className="text-danger">
              First name cannot exceed 10 characters
            </p>
          )}
          {errors?.firstName?.type === "pattern" && (
            <p className="text-danger">Alphabetical characters only</p>
          )}

          <label>Laste Name</label>
          <input
            className="form-control"
            {...register("lastName", { pattern: /^[A-Za-z]+$/i })}
          />
          {errors?.lastName?.type === "pattern" && (
            <p>Alphabetical characters only</p>
          )}

          <label>Age</label>
          <input
            className="form-control"
            {...register("age", { min: 18, max: 99, required: true })}
          />
          {errors.age && (
            <p className="text-danger">
              You Must be older then 18 and younger then 99 years old
            </p>
          )}
          <label>Email</label>
          <input
            className="form-control"
            {...register("email", {
              pattern:
                /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i,
              required: true,
            })}
          />
          {errors.email?.type === "required" && (
            <p className="text-danger">email id required</p>
          )}
          {errors?.email?.type === "pattern" && (
            <p className="text-danger">this is invalid email-id</p>
          )}

          <input type="submit" className="btn btn-success form-control mt-2" />
        </form>
      </div>
    </>
  );
}
