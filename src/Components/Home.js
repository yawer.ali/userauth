import React from "react";
import Navbar from "./Navbar";

export default function Home() {
  return (
    <>
      <Navbar />
      <section className="bgimage">
        <div className="container">
          <div className="row">
            <h2>Welcome To SmartStudy</h2>
            <p>Best Online Education Expertise</p>
            <div>
              <button className="btn btn-primary btn-sm mb-2">
                Get Started Here
              </button>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
