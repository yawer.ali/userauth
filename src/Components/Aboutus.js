import React from "react";
import Navbar from "./Navbar";

export default function Aboutus() {
  return (
    <>
    <Navbar/>
        <div className="container mt-2 text-center">
            <div className="text-info fw-bold h1">About Us</div>
            <p>Some text about who we are and what we do.</p>
            <p>
                Resize the browser window to see that way.
            </p>

            <h2>Our Team</h2>
                    <div className="row">
                        <div className="col-lg-3">
                            <div className="card">
                            <img src="https://images.unsplash.com/photo-1552374196-c4e7ffc6e126?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60" alt="Jane"/>
                            <div className="container">
                                <h2>Jane Doe</h2>
                                <p className="title">Founder</p>
                                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                                <p>jane@example.com</p>
                                <p><button className="button">Contact</button></p>
                            </div>
                            </div>
                        </div>
                            <div className="col-lg-3">
                            <div className="card">
                            <img src="https://images.unsplash.com/photo-1544168190-79c17527004f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NXx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60" alt="jhon"/>
                            <div className="container">
                                <h2>Jhon</h2>
                                <p className="title">CEO</p>
                                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                                <p>jhon@example.com</p>
                                <p><button className="button">Contact</button></p>
                            </div>
                            </div>
                        </div>

                        <div className="col-lg-3">
                            <div className="card">
                            <img src="https://images.unsplash.com/photo-1542596768-5d1d21f1cf98?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8OXx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60" alt="Lily"/>
                            <div className="container">
                                <h2>Lily</h2>
                                <p className="title">Web Tech</p>
                                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                                <p>lily@example.com</p>
                                <p><button className="button">Contact</button></p>
                            </div>
                            </div>
                        </div>

                        <div className="col-lg-3">
                            <div className="card">
                            <img src="https://images.unsplash.com/photo-1504199367641-aba8151af406?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MTF8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60" alt="martin"/>
                            <div className="container">
                                <h2>Martin</h2>
                                <p className="title">CEO & Founder</p>
                                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                                <p>martin@example.com</p>
                                <p><button className="button">Contact</button></p>
                            </div>
                            </div>
                        </div>

                    
                    </div>
            </div>
    </>
  );
}
