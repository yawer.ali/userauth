import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Navbar from "./Navbar";

const schema = yup
  .object({
    firstName: yup
      .string()
      .required("First Name is required")
      .matches(/^\S/, "First character cannot be Space ")
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field "),
    email: yup
      .string()
      .required("Email is required")
      .matches(/^\S/, "First character cannot be Space ")
      .matches(
        /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i,
        "valid email"
      )
      .email("Please enter a valid Email")
      .max(255),
    password: yup.number().positive().integer().required(),
  })
  .required();

export default function AnotherF() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    console.log(data);
  };
  return (
    <>
      <Navbar />

      <div className="container mt-2">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-3">
            <label htmlFor="firstName" className="form-label">
              First Name
            </label>
            <input
              type="text"
              className="form-control"
              {...register("firstName")}
            />
            <p className="text-danger">{errors.firstName?.message}</p>
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              {...register("email")}
            />
            <p className="text-danger">{errors.email?.message}</p>
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              {...register("password")}
            />
            <p className="text-danger">{errors.password?.message}</p>
          </div>
          <select className="form-select" aria-label="Default select example">
            <option selected>Country</option>
            <option value="1">India</option>
            <option value="2">England</option>
            <option value="3">Aus</option>
          </select>

          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" />
            <label className="form-check-label" htmlFor="Radio1">
              Male
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" />
            <label className="form-check-label" htmlFor="Radio2">
              Female
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" />
            <label className="form-check-label" htmlFor="Radio3">
              Other
            </label>
          </div>

          <div className="mb-3 form-check">
            <input type="checkbox" className="form-check-input" />
            <label className="form-check-label" htmlFor="Check">
              i Agree to terms & terms
            </label>
          </div>
          <button type="submit" className="btn btn-primary form-control">
            Submit
          </button>
        </form>
      </div>
    </>
  );
}
