import React from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "./Navbar";

export default function Dashboard() {
  const navigate = useNavigate();
  const removeToken = () => {
    localStorage.clear();
    navigate("/");
  };
  console.log("hello dashboard");
  return (
    <div>
      <Navbar />
      <div className="text-center mt-3">
        <h1 className="text-info">Dashboard</h1>
        <h5>HELLO</h5>
        <p>
          <button className="btn btn-primary btn-md mt-4" onClick={removeToken}>
            Logout
          </button>
        </p>
      </div>
    </div>
  );
}
