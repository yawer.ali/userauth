import React from "react";
import "./App.css";
// import Navbar from './Components/Navbar';
import Home from "./Components/Home";
import Aboutus from "./Components/Aboutus";
import Contactus from "./Components/Contactus";
import Register from "./Components/Register";
import Formv from "./Components/Formv";
import AnotherF from "./Components/AnotherF";
import Login from "./Components/Login";
import Dashboard from "./Components/Dashboard";
import ProtectedRoute from "./Components/ProtectedRoute";
// import Navbar from './Components/Navbar';
import { QueryClient, QueryClientProvider } from "react-query";
import {
  // HashRouter,  can also use
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

const queryClient = new QueryClient();

function App() {
  return (
    <div>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<Login />} />
            <Route
              path="/Home"
              element={
                <ProtectedRoute>
                  <Home />
                </ProtectedRoute>
              }
            />
            <Route
              path="/Register"
              element={
                <ProtectedRoute>
                  <Register />
                </ProtectedRoute>
              }
            />
            s
            <Route
              exact
              path="/Aboutus"
              element={
                <ProtectedRoute>
                  <Aboutus />
                </ProtectedRoute>
              }
            />
            <Route
              path="/Contactus"
              element={
                <ProtectedRoute>
                  <Contactus />
                </ProtectedRoute>
              }
            />
            <Route
              path="/Formv"
              element={
                <ProtectedRoute>
                  <Formv />{" "}
                </ProtectedRoute>
              }
            />
            <Route
              path="/AnotherF"
              element={
                <ProtectedRoute>
                  <AnotherF />
                </ProtectedRoute>
              }
            />
            <Route
              path="/Dashboard"
              element={
                <ProtectedRoute>
                  <Dashboard />
                </ProtectedRoute>
              }
            />
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    </div>
  );
}

export default App;
